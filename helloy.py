from flask import Flask, flash, render_template, redirect, request, session, abort

app = Flask(__name__)

@app.route("/")
def index():
    return "Index"

@app.route("/home")
def hello():
    return "Primeira aplicacao"

@app.route("/menbers")
def menbers():
    return "Menbers"

@app.route("/menbers/<string:name>/")
def getMenber(name):
    return render_template('template1.html', name=name) 

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=81)